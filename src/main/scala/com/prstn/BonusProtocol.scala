package com.prstn

import java.util.UUID

import com.prstn.CommonProtocol._
import shapeless.{:+:, CNil, Coproduct}

object CommonProtocol {
  type PlayerUUID = String
  type BonusID = Long
  type CommandID = String

  sealed trait PlayerUUIDAware {
    def playerUUID: PlayerUUID
  }

  sealed trait BonusIDAware {
    def bonusID: BonusID
  }

  sealed trait CommandIDAware {
    def commandID: CommandID
  }

  sealed trait FailureAware {
    def reason: String

    def invokeError(): Unit = {}
  }

  def newCommandId = UUID.randomUUID().toString
}

object BonusProtocol {

  trait BonusCommand extends CommandIDAware with BonusIDAware

  trait BonusCommandResponse extends CommandIDAware with BonusIDAware

  trait BonusCommandSuccess extends BonusCommandResponse

  trait BonusCommandsFailure extends BonusCommandResponse with FailureAware
  {
    override def invokeError(): Unit = {
      throw buildException(this)
    }
  }

  case class CreateBonus(commandID: CommandID, bonusEntity: AnyRef) extends CommandIDAware

  case class BonusCreated(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommandSuccess

  case class BonusNotCreated(override val commandID: CommandID, reason: String) extends BonusCommandsFailure {
    override def bonusID = -1L
  }

  case class BonusValidationFailed(override val commandID: CommandID, error: RuntimeException) extends BonusCommandsFailure {
    override def invokeError(): Unit = {
      throw error
    }

    override def reason: String = ""

    override def bonusID: BonusID = -1L
  }

  case class Start(override val commandID: CommandID, override val bonusID: BonusID, override val playerUUID: PlayerUUID) extends BonusCommand with PlayerUUIDAware

  case class Cancel(override val commandID: CommandID, override val bonusID: BonusID, reason: String, cancellerUUID: String = null) extends BonusCommand

  case class Kill(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommand

  case class Expire(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommand

  case class WakeUp(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommand

  case class BonusUpdated(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommandSuccess

  case class BonusNotUpdated(override val commandID: CommandID, override val bonusID: BonusID, reason: String) extends BonusCommandsFailure

  case class BonusNotFound(override val commandID: CommandID, override val bonusID: BonusID) extends BonusCommandsFailure {
    override def reason: String = ""

    override def invokeError(): Unit = {
      throw new Exception(buildException(this))
    }
  }

  private def buildException(response: Any) = new RuntimeException(String.format("%s: %s", response.getClass.getSimpleName, response.asInstanceOf[BonusProtocol.BonusCommandsFailure].reason))

}
