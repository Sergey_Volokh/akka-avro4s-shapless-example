package com.prstn

import java.time.LocalDateTime
import javax.money.{CurrencyUnit, MonetaryAmount}

import com.prstn.CommonProtocol.BonusID
import shapeless.:+:

object BonusEvents {

  type BrandId = String
  type EventID = String
  type PlayerUUID = String
  type ProviderID = String
  type AggregatorID = String
  type BonusUUID = String
  type CurrencyUnitID = String


  sealed trait BrandIdAware {
    def brandId: BrandId
  }

  sealed trait BonusIDAware {
    def bonusID: BonusID
  }

  sealed trait CurrencyUnitIDAware {
    def currency: CurrencyUnitID
  }

  sealed trait EventIDAware {
    def eventID: EventID
  }

  sealed trait ProviderIDAware {
    def gameProviderId: ProviderID
  }

  sealed trait AggregatorIDAware {
    def gameAggregatorId: AggregatorID
  }

  sealed trait GameEvent extends ProviderIDAware with AggregatorIDAware {
    def bonusGameEventDelta: String

    def currentBonusAmount: String

    def wagered: String
  }

  sealed trait PlayerUUIDAware {
    def playerUUID: PlayerUUID
  }

  sealed trait BonusUUIDAware {
    def bonusUUID: BonusUUID
  }

  sealed trait StartingState

  sealed trait InProgressState

  sealed trait StoppingState

  sealed trait BonusStateAware extends InProgressState

  sealed trait BonusEvent extends EventIDAware with BonusIDAware with PlayerUUIDAware with BrandIdAware

  case class BaseBonusEvent(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent
  case class BonusSelfEvicted(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent

  case class BonusStarting(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with StartingState

  case class WithdrawLocked(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with StartingState

  case class BonusDeposited(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with StartingState

  case class BonusStarted(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusUUID: BonusUUID, override val bonusID: BonusID , grantedAmount: MonetaryAmount, startDate: LocalDateTime, expirationDate: LocalDateTime)
    extends BonusEvent with InProgressState with BonusUUIDAware

  case class BetApplied(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusUUID: BonusUUID, override val bonusID: BonusID,
                        gameProviderId: ProviderID, gameAggregatorId: AggregatorID, stake: String, walletBalance: String, override val bonusGameEventDelta: String,
                        override val currentBonusAmount: String, override val wagered: String,
                        gameId: String, gameSessionUUID: String, gameRoundId: String)
    extends BonusEvent with GameEvent with InProgressState with BonusUUIDAware

  case class WinApplied(override val eventID: EventID, gameProviderId: ProviderID, gameAggregatorId: AggregatorID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusUUID: BonusUUID, override val bonusID: BonusID,
                        win: String, override val bonusGameEventDelta: String, override val currentBonusAmount: String, override val wagered: String,
                        gameId: String, gameSessionUUID: String, gameRoundId: String)
    extends BonusEvent with GameEvent with InProgressState with BonusUUIDAware

  case class WageringCompleted(override val eventID: EventID, gameProviderId: ProviderID, gameAggregatorId: AggregatorID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusUUID: BonusUUID, override val bonusID: BonusID, balanceAmount: String) extends BonusEvent with BonusStateAware with BonusUUIDAware

  case class BonusConsumed(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with BonusStateAware

  case class BonusExpired(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with BonusStateAware

  case class BonusCanceled(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID, reason: String, cancellerUUID: String = null) extends BonusEvent with BonusStateAware

  case class BonusStopping(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID, event: BonusStateAware) extends BonusEvent with StoppingState

  case class WalletAdjusted(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID) extends BonusEvent with StoppingState

  case class WithdrawUnlocked(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusID: BonusID, override val currency: CurrencyUnitID) extends BonusEvent with StoppingState with CurrencyUnitIDAware

  case class BonusStopped(override val eventID: EventID, override val playerUUID: PlayerUUID, override val brandId: BrandId, override val bonusUUID: BonusUUID, override val bonusID: BonusID, override val currency: CurrencyUnitID) extends BonusEvent with StoppingState  with BonusUUIDAware with CurrencyUnitIDAware

  import shapeless.{Coproduct, :+:, CNil}

  case class BonusEventsWrapper(value: Option[BonusType])

  //  type BonusType =  BonusSelfEvicted  :+:
  //    BonusStarting     :+: WithdrawLocked  :+: BonusDeposited  :+:
  //    BonusStarted    :+: BetApplied      :+: WinApplied      :+:
  //    WageringCompleted :+: BonusConsumed   :+: BonusExpired    :+:    BonusCanceled     :+: BonusStopping   :+: WalletAdjusted  :+:
  //    WithdrawUnlocked  :+: BonusStopped   :+: CNil

  // BonusStarted +
  // BonusStarting +
  // BonusSelfEvicted +
  // BonusStopped -
  // BetApplied +
  // WinApplied +
  // WageringCompleted +
  // WithdrawUnlocked +
  // BonusConsumed +
  // BonusExpired +
  // BonusCanceled +
  // WithdrawLocked +
  // WalletAdjusted +


  type BonusType = BonusStarting :+:  BonusStarted  :+: BonusStopping :+:
    BonusSelfEvicted  :+: BonusDeposited :+:
    BonusStopped :+: BetApplied :+: WinApplied :+: WageringCompleted :+: WithdrawUnlocked :+:
    BonusConsumed :+: BonusExpired :+: BonusCanceled :+:
    WithdrawLocked :+: WalletAdjusted :+: CNil

  //    TRUE EVENTS
  //    BonusStarted :+: BonusStarting :+:
  //    BonusSelfEvicted :+: BonusExpired :+: BonusDeposited :+:
  //    BonusConsumed :+: BonusCanceled :+:
  //    WithdrawLocked :+: WalletAdjusted :+: CNil

  // Problem events
  //    :+: BonusStopping      other event inside
  //    :+: BonusStopped        CurrencyUnitIDAware +
  //    :+: BetApplied          MonetaryAmount +
  //    :+: WinApplied          MonetaryAmount +
  //    :+: WageringCompleted   MonetaryAmount +
  //    :+: WithdrawUnlocked    CurrencyUnitIDAware +
}