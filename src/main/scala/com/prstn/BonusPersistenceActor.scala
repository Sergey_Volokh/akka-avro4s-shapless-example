package com.prstn

import java.time.LocalDateTime

import akka.actor.ActorLogging
import akka.persistence.PersistentActor
import com.prstn.BonusEvents._
import org.javamoney.moneta.Money
import shapeless.{Coproduct, Inl}


class BonusPersistenceActor extends PersistentActor with ActorLogging {

  override def receiveRecover: Receive = {
    case recover => println(s"BonusPersistenceActor receiveRecover: $recover")
  }

  override protected def onRecoveryFailure(cause: Throwable, event: Option[Any]): Unit = {
    println(s"<< onRecoveryFailure $cause - $event")
  }

  override def receiveCommand: Receive = {
    case "BonusStarted" =>
      persist(BonusStarted("2344", "uuid2", "bramd2", "bonus2UUid", 2L, Money.parse("USD 1"), LocalDateTime.now(), LocalDateTime.now())) { it =>
        println(s"BonusStarted = $it")
      }
    case "BonusStarting" =>
      persist(BonusStarting("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"BonusStarting = $it")
      }
    case "BonusSelfEvicted" =>
      persist(BonusSelfEvicted("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"BonusSelfEvicted = $it")
      }
    case "BonusStopped" =>
      persist(BonusStopped("2344", "uuid2", "bramd2", "bonusuuid", 1, "")) { it =>
        println(s"BonusStopped = $it")
      }
    case "BetApplied" =>
      persist(BetApplied("2344", "uuid2", "bramd2", "bonusuuid", 1, "gameprovider", "gameaggreagator", "10 USD", "10 USD", "0 USD", "0 USD", "0 USD", "0 USD", "", "")) { it =>
        println(s"BetApplied = $it")
      }
    case "WinApplied" =>
      persist(WinApplied("2344", "uuid2", "bramd2", "bonusuuid", "", "gameprovider", 1, "10 USD", "10 USD", "0 USD", "0 USD", "0 USD", "0 USD", "")) { it =>
        println(s"WinApplied = $it")
      }
    case "WageringCompleted" =>
      persist(WageringCompleted("2344", "uuid2", "bramd2", "playerUUID", "", "", 1, "")) { it =>
        println(s"WageringCompleted = $it")
      }

    case "WithdrawUnlocked" =>
      persist(WithdrawUnlocked("2344", "uuid2", "bramd2", 1, "")) { it =>
        println(s"WithdrawUnlocked = $it")
      }
    case "BonusExpired" =>
      persist(BonusExpired("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"BonusExpired = $it")
      }
    case "BonusConsumed" =>
      persist(BonusConsumed("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"BonusConsumed = $it")
      }
    case "BonusCanceled" =>
      persist(BonusCanceled("2344", "uuid2", "bramd2", 1, "", "")) { it =>
        println(s"BonusCanceled = $it")
      }
    case "WithdrawLocked" =>
      persist(WithdrawLocked("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"WithdrawLocked = $it")
      }
    case "WalletAdjusted" =>
      persist(WalletAdjusted("2344", "uuid2", "bramd2", 1)) { it =>
        println(s"WalletAdjusted = $it")
      }
    case _ => println("any")
  }

  override def persistenceId: String = "test-id321"
}
