package com.prstn

import akka.actor.{ActorRef, ActorSystem, Props}
import com.prstn.BonusEvents.BonusStarted
import com.typesafe.config.ConfigFactory

object App extends App {
  val actorSystem: ActorSystem = ActorSystem("BonusActorSystem", ConfigFactory.load())
  val bonusActor: ActorRef = actorSystem.actorOf(Props[BonusPersistenceActor], "persistent-actor")

  bonusActor ! "BonusStarting"
  bonusActor ! "BonusStarted"
  bonusActor ! "BonusSelfEvicted"
  bonusActor ! "BonusStopped"
  bonusActor ! "BetApplied"
  bonusActor ! "WinApplied"
  bonusActor ! "WageringCompleted"
  bonusActor ! "WithdrawUnlocked"
  bonusActor ! "BonusConsumed"
  bonusActor ! "BonusExpired"
  bonusActor ! "BonusCanceled"
  bonusActor ! "WithdrawLocked"
  bonusActor ! "WalletAdjusted"
  bonusActor ! "WalletAdjusted"
  bonusActor ! "WalletAdjusted"

  Thread.sleep(5000)

  actorSystem.terminate()
}
