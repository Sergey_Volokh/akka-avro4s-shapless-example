package com.prstn.serializer

import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import javax.money.{CurrencyUnit, Monetary, MonetaryAmount}

import akka.serialization.SerializerWithStringManifest
import com.sksamuel.avro4s.{FromRecord, FromValue, ToValue, _}
import com.prstn.BonusEvents._
import com.prstn.BonusEvents
import org.apache.avro.Schema
import org.apache.avro.Schema.Field
import org.javamoney.moneta.Money
import shapeless.ops.hlist.Union
import shapeless.{Coproduct, Generic, Inl, Inr}

class AkkaAvroSerializer extends SerializerWithStringManifest {

  import com.prstn.serializer.Avro4sImplicitConverters._

  override def manifest(o: AnyRef): String = o.getClass.getName

  implicit val schemaFor: SchemaFor[BonusEventsWrapper] = SchemaFor[BonusEventsWrapper]

  override def identifier: Int = 1231223

  final val Manifest = classOf[BonusEventsWrapper].getName

  override def toBinary(o: AnyRef): Array[Byte] = {
    val output = new ByteArrayOutputStream
    o match {
      case e: BonusStarted  => serializer(output, Coproduct[BonusType](e))
      case e: BonusStarting => serializer(output, Coproduct[BonusType](e))
      case e: BonusSelfEvicted => serializer(output, Coproduct[BonusType](e))
      case e: BonusStopped => serializer(output, Coproduct[BonusType](e))
      case e: BetApplied => serializer(output, Coproduct[BonusType](e))
      case e: WinApplied => serializer(output, Coproduct[BonusType](e))
      case e: WageringCompleted => serializer(output, Coproduct[BonusType](e))
      case e: WithdrawUnlocked => serializer(output, Coproduct[BonusType](e))
      case e: BonusConsumed => serializer(output, Coproduct[BonusType](e))
      case e: BonusExpired => serializer(output, Coproduct[BonusType](e))
      case e: BonusCanceled => serializer(output, Coproduct[BonusType](e))
      case e: WithdrawLocked => serializer(output, Coproduct[BonusType](e))
      case e: WalletAdjusted => serializer(output, Coproduct[BonusType](e))
    }
    output.toByteArray
  }

  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = {
    val is = AvroInputStream.binary[BonusEventsWrapper](bytes)
    val event = is.iterator().toList
    val result = event.head.value.head
    is.close()
    val format = RecordFormat[BonusEventsWrapper]
    result
  }

  private def serializer(output: ByteArrayOutputStream, e: BonusType): Unit = {
    val avro = AvroOutputStream.binary[BonusEventsWrapper](output)
    avro.write(BonusEventsWrapper(Some(e)))
    avro.close()
  }
}
