package com.prstn.serializer

import java.time.LocalDateTime
import java.util
import java.util.List
import javax.money.MonetaryAmount

import com.prstn.BonusEvents.{BaseBonusEvent, BonusEvent, BonusStarted}
import com.sksamuel.avro4s._
import org.apache.avro.Schema
import org.apache.avro.Schema.Field
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.javamoney.moneta.Money

object Avro4sImplicitConverters {

  implicit object LocalDateTimeToSchema extends ToSchema[LocalDateTime] {
    override val schema: Schema = Schema.create(Schema.Type.STRING)
  }

  implicit object LocalDateTimeToValue extends ToValue[LocalDateTime] {
    override def apply(value: LocalDateTime): String = {
      value.toString
    }
  }

  implicit object LocalDateTimeFromValue extends FromValue[LocalDateTime] {
    override def apply(value: Any, field: Field): LocalDateTime = {
      LocalDateTime.parse(value.toString)
    }
  }

  implicit object MonetaryAmountToSchema extends ToSchema[MonetaryAmount] {
    override val schema: Schema = Schema.create(Schema.Type.STRING)
  }

  implicit object MonetaryAmountToValue extends ToValue[MonetaryAmount] {
    override def apply(value: MonetaryAmount): String = {
      value.toString
    }
  }

  implicit object MonetaryAmountFromValue extends FromValue[MonetaryAmount] {
    override def apply(value: Any, field: Field): MonetaryAmount = {
      Money.parse(value.toString)
    }
  }

}
