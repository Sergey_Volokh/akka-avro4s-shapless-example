name := "akka-avro-persistance-demo"

version := "0.1"
scalaVersion := "2.11.11"

val akkaVersion = "2.5.4"
val avroVersion = "1.8.0"

libraryDependencies += "com.typesafe.akka"          %%  "akka-actor"              % akkaVersion
libraryDependencies += "com.typesafe.akka"          %%  "akka-persistence"        % akkaVersion
libraryDependencies += "com.typesafe.akka"          %%  "akka-persistence-query"  % akkaVersion

libraryDependencies += "org.iq80.leveldb"           %   "leveldb"                 % "0.7"
libraryDependencies += "org.fusesource.leveldbjni"  %   "leveldbjni-all"          % "1.8"

libraryDependencies += "com.sksamuel.avro4s"        %   "avro4s-core_2.11"        % "1.8.0"

libraryDependencies += "org.apache.avro"            %   "avro"                    % avroVersion

libraryDependencies += "org.slf4j"                  %   "slf4j-api"                 % "1.7.25"
libraryDependencies += "com.typesafe.akka"          %   "akka-slf4j_2.11"           % "2.5.4"
libraryDependencies += "org.slf4j"                  % "slf4j-log4j12"               % "1.7.25"
libraryDependencies += "org.javamoney"              % "moneta"                      % "1.1"

